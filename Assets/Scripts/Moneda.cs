﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moneda : MonoBehaviour {

    //fields
    public static int contador = 0;

    private void Awake()
    {
        contador = 0;
    }

    // Use this for initialization
    void Start () {
        contador++;
        Debug.Log(contador);
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.Rotate(2, 0, 0);
    }

    private void OnDestroy()
    {
        contador--;
        if (contador <= 0) //win condition
        {
            GameObject[] fireworks = GameObject.FindGameObjectsWithTag("Fire"); 
            foreach (GameObject fire in fireworks)
            {
                fire.GetComponent<ParticleSystem>().Play();
            }
            GameObject chronoObject = GameObject.FindWithTag("Time");
            Destroy(chronoObject);
        }


        
    }
    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }
}
